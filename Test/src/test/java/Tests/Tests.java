package Tests;

import Pages.HomePage;
import helpers.Helpers;
import Pages.LoginPages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Tests {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {
        DesiredCapabilities caps = new DesiredCapabilities();
        System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://trello.com/login");
        Helpers helpers = new Helpers();
        helpers.PauseSleep(5);
    }

    @Test(priority = 1)
    public void loginIncorrecto() throws InterruptedException {
        LoginPages loginPages = new LoginPages(driver);
        loginPages.loginError("user","user");
        Assert.assertTrue(loginPages.validaMensajeErrorEnLogin());
    }

    @Test(priority = 2)
    public void loginCorrecto() throws InterruptedException {
        LoginPages loginPages = new LoginPages(driver);
        loginPages.login("svirtualtest@gmail.com", "svirtual.2020");
        Assert.assertTrue(loginPages.validaLoginCorrecto());
    }

    @Test(priority = 3)
    public void flujoDeCreacionDeTableroYModificacion() throws InterruptedException{
       LoginPages loginPages = new LoginPages(driver);
       loginPages.login("svirtualtest@gmail.com", "svirtual.2020");
        HomePage homePage = new HomePage(driver);
        homePage.crearTablero();
        Assert.assertTrue(homePage.validaTablero());

        //homePage.crearListas();
        homePage.crearTarjeta();
        Assert.assertTrue(homePage.validaListaYTarjeta());

        homePage.editarTarjeta();
        Assert.assertTrue(homePage.validaEditar());

        Assert.assertTrue(homePage.moverTarjeta());

        homePage.eliminarTablero();
        Assert.assertTrue(homePage.validaTableroEliminado());

        homePage.verificarTablero();
    }
    @AfterMethod
    public void tearDown(){
        driver.close();
    }
}