package Pages;

import helpers.Helpers;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private WebDriver driver;

    //******* INICILIZANDO DRIVER ********\\

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    //******* XPATHS CON VARIABLE ASIGNADA ******\\

    @FindBy(xpath = "(//*[@class='sc-bdVaJa kBFJig'])[5]")
    private WebElement boton_mas_home;

    @FindBy(xpath = "(//*[@class='_2FCfpANq784raH'])[1]")
    private WebElement boton_crear_tablero;

    @FindBy(xpath = "(//*[@class='_23NUW98LaZfBpQ'])[1]")
    private WebElement titulo_tablero;

    @FindBy(xpath = "(//*[@class='uj9Ovoj4USRUQz voB8NatlbuEme5 _21upOlzpUQJcdT'])[1]")
    private WebElement boton_crear_tabla;

    @FindBy(xpath = "(//*[@class='list-name-input'])")
    private WebElement etiqueta_input;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary mod-list-add-button js-save-edit'])")
    private WebElement boton_crear_etiqueta;

    @FindBy(xpath = "(//*[@class='icon-sm icon-add'])[1]")
    private WebElement boton_anadir_tarjeta;

    @FindBy(xpath = "(//*[@class='list-card-composer-textarea js-card-title'])")
    private WebElement text_area_tarjeta;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary confirm mod-compact js-add-card'])")
    private WebElement boton_crear_tarjeta;

    @FindBy(xpath = "(//*[@class='list-card-title js-card-name'])")
    private WebElement boton_editar_tarjeta;

    @FindBy(xpath = "(//*[@class='field field-autosave js-description-draft description card-description'])")
    private WebElement text_area_descripcion;

    @FindBy(xpath = "(//*[@class='button-link js-edit-labels'])")
    private WebElement boton_etiquetas;

    @FindBy(xpath = "(//*[@class='card-label mod-selectable card-label-green  js-select-label selected'])[1]")
    private WebElement boton_color_etiqueta_verde;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary confirm mod-submit-edit js-save-edit'])")
    private WebElement boton_guardar_descripcion;

    @FindBy(xpath = "(//*[@class='button-link js-add-due-date'])")
    private WebElement boton_agregar_vencimiento;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary wide confirm'])")
    private WebElement boton_guardar_vencimiento;

    @FindBy(xpath = "(//*[@class='button-link js-attach'])")
    private WebElement boton_adjunto;

    @FindBy(xpath = "(//*[@class='attachment-add-link-input js-attachment-url js-autofocus'])")
    private WebElement input_adjunto;

    @FindBy(xpath = "(//*[@class='js-add-attachment-url'])")
    private WebElement boton_agregar_adjunto;

    @FindBy(xpath = "(//*[@class='comment-box-input js-new-comment-input'])")
    private WebElement boton_escribir_comentario;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary confirm mod-no-top-bottom-margin js-add-comment'])")
    private WebElement boton_guardar_comentario;

    @FindBy(xpath = "(//*[@class='icon-md icon-close dialog-close-button js-close-window dialog-close-button-light'])")
    private WebElement boton_cerrar_edicion;

    @FindBy(xpath = "(//*[@class='board-menu-navigation-item-link js-open-more'])")
    private WebElement boton_menu_mas;

    @FindBy(xpath = "(//*[@class='board-menu-navigation-item-link js-close-board'])")
    private WebElement boton_cerrar_tablero;

    @FindBy(xpath = "(//*[@class='js-confirm full nch-button nch-button--danger'])")
    private WebElement boton_cerrar;

    @FindBy(xpath = "(//*[@class='quiet js-delete'])")
    private WebElement boton_cerrar_permanente;

    @FindBy(xpath = "(//*[@class='js-confirm full nch-button nch-button--danger'])")
    private WebElement boton_eliminar;

    @FindBy(xpath = "(//*[@class='sc-bdVaJa kBFJig'])[2]")
    private WebElement boton_home;

    @FindBy(xpath = "(//*[@class='js-autofocus js-label-search'])")
    private WebElement input_color_etiqueta;

    @FindBy(xpath = "(//*[@class='pop-over-header-close-btn icon-sm icon-close'])")
    private WebElement boton_cerrar_etiqueta;

    @FindBy(xpath = "(//*[@class='list-header-extras-menu dark-hover js-open-list-menu icon-sm icon-overflow-menu-horizontal'])")
    private WebElement boton_opciones_tarjeta;

    @FindBy(xpath = "(//*[@class='js-move-list'])")
    private WebElement boton_opcion_mover;

    @FindBy(xpath = "(//*[@class='js-select-list-pos'])")
    private WebElement boton_mover_posicion;

    @FindBy(xpath = "(//*[@class='nch-button nch-button--primary wide js-commit-position'])")
    private WebElement boton_mover_tarjeta;

    @FindBy(xpath = "(//*[@class='board-header-btn mod-show-menu js-show-sidebar'])")
    private WebElement boton_abrir_menu;

    //**** XPATH PARA VALIDACIONES CON VARIABLE ASIGNADA ****\\

    @FindBy(xpath = "(//*[@class='js-board-editing-target board-header-btn-text'])")
    private WebElement nombre_tablero_txt;

    @FindBy(xpath = "(//*[@class='list-card-title js-card-name'])")
    private WebElement nombre_tarea_txt;

    @FindBy(xpath = "(//*[@class='list-card-title js-card-name'])")
    private WebElement nombre_tarea_editada;

    @FindBy(xpath = "(//*[@class='badge-icon icon-sm icon-attachment'])")
    private WebElement barra_editada;

    @FindBy(xpath = "(//*[@class='quJQh3DmirLZ_k _3aT9LJlqB4u3pW'])")
    private WebElement mensaje_tablero_no_existe;

    //******METODOS CON SU VALIDACION ******\\

    public void crearTablero() {
        boton_mas_home.click();
        boton_crear_tablero.click();
        Helpers helpers = new Helpers();
        helpers.PauseSleep(3);
        titulo_tablero.sendKeys("Javier Bahamondez");
        helpers.PauseSleep(1);
        boton_crear_tabla.click();
        helpers.PauseSleep(5);
    }

    public boolean validaTablero(){
        try{
            String nombre = "Javier Bahamondez";
            String nombre_tablero_pagina = nombre_tablero_txt.getText();
            return nombre.equals(nombre_tablero_pagina);
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /*public void crearListas(){
        Helpers helpers = new Helpers();
        helpers.PauseSleep(2);
        crearEtiquetas(etiqueta_input, "Tareas");
        boton_crear_etiqueta.click();
        helpers.PauseSleep(3);
        crearEtiquetas(etiqueta_input, "En proceso");
        boton_crear_etiqueta.click();
        helpers.PauseSleep(3);
        crearEtiquetas(etiqueta_input, "Hecho");
        boton_crear_etiqueta.click();
    }
*/
    public void crearTarjeta(){
        Helpers helpers = new Helpers();
        //boton_anadir_tarjeta.click();
        helpers.PauseSleep(2);
        text_area_tarjeta.sendKeys("Prueba Automatizacion");
        helpers.PauseSleep(2);
        boton_crear_tarjeta.click();
        helpers.PauseSleep(2);
    }

    public boolean validaListaYTarjeta(){
        try{
            String tarea = "Prueba Automatizacion";
            String nombre_tarea_pagina = nombre_tarea_txt.getText();
            return tarea.equals(nombre_tarea_pagina);
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void editarTarjeta(){
        anadirDescripcion();
        seleccionarEtiquetaVerde();
        agregarVecimiento();
        adjuntarImagen();
        anadirComentario();
        cerrarYGuardar();
    }

    public void anadirDescripcion(){
        Helpers helpers = new Helpers();
        helpers.PauseSleep(2);
        boton_editar_tarjeta.click();
        helpers.PauseSleep(2);
        text_area_descripcion.sendKeys("Hola, soy Javier Bahamondez y esto es un test para Automatizador QA.");
        helpers.PauseSleep(3);
    }

    public void seleccionarEtiquetaVerde(){
        Helpers helpers = new Helpers();
        boton_etiquetas.click();
        helpers.PauseSleep(2);
        input_color_etiqueta.sendKeys("green" + Keys.ENTER);
        helpers.PauseSleep(2);
        boton_cerrar_etiqueta.click();
        helpers.PauseSleep(2);
    }

    public void agregarVecimiento(){
        Helpers helpers = new Helpers();
        boton_agregar_vencimiento.click();
        helpers.PauseSleep(5);
        boton_guardar_vencimiento.click();
        helpers.PauseSleep(2);
    }

    public void adjuntarImagen(){
        Helpers helpers = new Helpers();
        boton_adjunto.click();
        helpers.PauseSleep(2);
        input_adjunto.sendKeys("https://www.ecestaticos.com/image/clipping/557/418/79776773aab795837282c7d4947abaf7/por-que-nos-parece-que-los-perros-sonrien-una-historia-de-30-000-anos.jpg");
        helpers.PauseSleep(1);
        boton_agregar_adjunto.click();
        helpers.PauseSleep(2);
    }

    public void anadirComentario(){
        Helpers helpers = new Helpers();
        boton_escribir_comentario.sendKeys("Perro loco");
        helpers.PauseSleep(2);
        boton_guardar_comentario.click();
        helpers.PauseSleep(1);
    }
    public void cerrarYGuardar(){
        Helpers helpers = new Helpers();
        helpers.PauseSleep(2);
        boton_cerrar_edicion.click();
    }

    public boolean validaEditar(){
        try{
            return barra_editada.isDisplayed();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean moverTarjeta(){
        try {
            Helpers helpers = new Helpers();
            boton_opciones_tarjeta.click();
            boton_opcion_mover.click();
            helpers.PauseSleep(1);
            helpers.selectDropdown(boton_mover_posicion, "2");
            helpers.PauseSleep(1);
            boton_mover_tarjeta.click();
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public void eliminarTablero(){
        Helpers helpers = new Helpers();
        //boton_abrir_menu.click();
        boton_menu_mas.click();
        helpers.PauseSleep(2);
        boton_cerrar_tablero.click();
        helpers.PauseSleep(2);
        boton_cerrar.click();
        helpers.PauseSleep(2);
        boton_cerrar_permanente.click();
        helpers.PauseSleep(2);
        boton_eliminar.click();
        helpers.PauseSleep(2);
    }

    public boolean validaTableroEliminado() {
        try {
            return mensaje_tablero_no_existe.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void verificarTablero(){
        boton_home.click();
    }

    public void crearEtiquetas(WebElement element,String nombreEtiqueta){
        element.sendKeys(nombreEtiqueta);
    }
}