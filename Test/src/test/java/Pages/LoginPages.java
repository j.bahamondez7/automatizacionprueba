package Pages;

import helpers.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPages {
    private WebDriver driver;

    //****** INICIALIZANDO DRIVER *******\\

    public LoginPages(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    //****** XPATHS CON VARIABLE ASIGNADA ******\\

    @FindBy(id = "user")
    private WebElement user_input;

    @FindBy(id = "password")
    private WebElement password_input;

    @FindBy(id = "login")
    private WebElement btn_login;

    @FindBy(id = "login-submit")
    private WebElement btn_login2;

    //**** XPATHS PARA VALIDACIONES CON VARIABLE ASIGNADA ****\\

    @FindBy(id = "error")
    private WebElement error_mensaje;

    @FindBy(xpath = "(//*[@class='sc-bdVaJa kBFJig'])[5]")
    private WebElement boton_mas_home;

    //***** METODOS CON SU VALIDACION ******\\

    public void login(String user, String pass) {
        user_input.sendKeys(user);
        Helpers helpers = new Helpers();
        helpers.PauseSleep(5);
        btn_login.click();
        password_input.sendKeys(pass);
        btn_login2.click();
        helpers.PauseSleep(5);
    }

    public void loginError(String user, String pass) {
        user_input.sendKeys(user);
        password_input.sendKeys(pass);
        btn_login.click();
        Helpers helpers = new Helpers();
        helpers.PauseSleep(3);
    }

    public boolean validaMensajeErrorEnLogin() {
        try {
            return error_mensaje.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
    public boolean validaLoginCorrecto() {
        try {
           return boton_mas_home.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}


